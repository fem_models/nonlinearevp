// Copyright (C) 2018 Guillaume Demésy
//
// This file is part of the model NonLinearEVP.pro.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with This program. If not, see <https://www.gnu.org/licenses/>.

Include "NonLinearEVP_data.geo";

Group {
  // Domains
  Om_1      = Region[100] ; //in (dispersive)
  Om_2inter = Region[101]; // out (non dispersive)
  pmltop    = Region[102]; // pml top
  pmlbot    = Region[103]; // pml top
  Om        = Region[{Om_1,Om_2inter,pmltop,pmlbot}];
  Om_nopml  = Region[{Om_1,Om_2inter}];
  Om_pml    = Region[{pmltop,pmlbot}];
  Om_2      = Region[{Om_pml,Om_2inter}]; // non dispersive

  // Boundaries
  SurfBlochLeft   = Region[1001];
  SurfBlochRight  = Region[1002];
  Surfpml         = Region[{1003,1004}];
  Bound           = Region[{1005}];
  PrintPoint      = Region[10000];
}

Function{
  I[]   = Complex[0,1];

  siwt  =   -1.;
  a_pml =    1.;
  b_pml = -siwt;

  sx[Om_pml]      = 1.;
  sy[Om_pml]      = Complex[a_pml,b_pml];
  sx[Om_nopml]    = 1.;
  sy[Om_nopml]    = 1.;

  sz = 1;

  PML_Tensor[] = TensorDiag[sz*sy[]/sx[],sx[]*sz/sy[],sx[]*sy[]/sz];
  mur[]        = TensorDiag[1,1,1]*PML_Tensor[];
  epsr_nod[]   = TensorDiag[1,1,1]*PML_Tensor[];

  term_o3_3cc_1[]      = Complex[-eps_oo_1,0];
  term_o3_2cc_1[]      = Complex[0,-eps_oo_1*gam_1];
  term_o3_1cc_1[]      = Complex[om_d_1^2,0];
  term_o3_1rr_1[]      = cel^2*Complex[1,0];
  term_o3_0rr_1[]      = cel^2*Complex[0,gam_1];
  term_o3_3cc_2[]      = -1*epsr_nod[];
  term_o3_2cc_2[]      = -1*epsr_nod[]*Complex[0,gam_2];
  term_o3_1cc_2[]      = Complex[om_d_2^2,0];
  term_o3_1rr_2[]      = cel^2*Complex[1,0];
  term_o3_0rr_2[]      = cel^2*Complex[0,gam_2];
  term_o3_3cc[Om_1] = term_o3_3cc_1[];
  term_o3_2cc[Om_1] = term_o3_2cc_1[];
  term_o3_1cc[Om_1] = term_o3_1cc_1[];
  term_o3_1rr[Om_1] = term_o3_1rr_1[];
  term_o3_0rr[Om_1] = term_o3_0rr_1[];
  term_o3_3cc[Om_2] = term_o3_3cc_2[];
  term_o3_2cc[Om_2] = term_o3_2cc_2[];
  term_o3_1cc[Om_2] = term_o3_1cc_2[];
  term_o3_1rr[Om_2] = term_o3_1rr_2[];
  term_o3_0rr[Om_2] = term_o3_0rr_2[];


  dephx[] = Complex[ Cos[kx*a_lat] , Sin[kx*a_lat]];

  // dummy filter (better to use slepc to filter)
  EigFilter[] = (Norm[$EigenvalueReal] > 1e-20);

  //Auxialiary Field resolution coefficients
  eps_inf[] = TensorDiag[1.,1.,1.]*eps_oo_1;
  Om_D[]    = TensorDiag[1.,1.,1.]*om_d_1/Sqrt[2.];
  Gamma_d[] = TensorDiag[1.,1.,1.]*gam_1;
}

Constraint {
  {Name BlochX;
    Case {
      { Region SurfBlochRight; Type LinkCplx ; RegionRef SurfBlochLeft;
            Coefficient dephx[]; Function Vector[$X-a_lat,$Y,$Z] ;
      }
    }
  }

  {Name Dirichlet; Type Assign;
    Case {
      { Region Surfpml ; Value 0.; }
    }
  }
  // div condition
  { Name Constraint_cInt; Type Assign;
    Case {
    }
  }
  { Name Constraint_Int; Type Assign;
    Case {
      { Region Bound; Value 0.; }
    }
  }
}

Jacobian {
  {Name JVol ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name JSur ;
    Case{
      { Region All ; Jacobian Sur ; }
    }
  }
  { Name JLin ;
    Case{
      { Region All ; Jacobian Lin ; }
    }
  }
}

Integration {
  { Name Int_1 ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point    ; NumberOfPoints  1 ; }
          { GeoElement Line     ; NumberOfPoints  4 ; }
          { GeoElement Line2    ; NumberOfPoints  4 ; }
          { GeoElement Line3    ; NumberOfPoints  4 ; }
          { GeoElement Triangle ; NumberOfPoints  6 ; }
          { GeoElement Triangle2; NumberOfPoints  12 ; }
          { GeoElement Triangle3; NumberOfPoints  12 ; }
        }
      }
    }
  }
}

FunctionSpace {
  { Name Hgrad_perp; Type Form1P;  
    BasisFunction {
      // // for second order mesh elements - Lagrange P3 :
      // un   : BF_Node    // order 2 is automatic (since all nodes are in NodesOf, even mid-edge ones)
      // un3e : BF_Node_3E // we add part of order 3 basis functions 
      // un3f : BF_Node_3F // we add the rest of order 3 basis functions 
      { Name sn;  NameOfCoef un;  Function BF_PerpendicularEdge_1N; Support Region[Om]; Entity NodesOf[All]; }
      If(flag_og==1) 
        // for curved elements, mid-edge nodes are already included in BF_PerpendicularEdge_1N
        // But for Mesh.ElementOrder=1, we need to add them explicitely 
        { Name sn2e; NameOfCoef un2e; Function BF_PerpendicularEdge_2E; Support Region[Om]; Entity EdgesOf[All]; }
      EndIf
      If(flag_o2i==1)
        { Name sn3e; NameOfCoef un3e; Function BF_PerpendicularEdge_3E; Support Region[Om]; Entity EdgesOf[All]; }
        { Name sn3f; NameOfCoef un3f; Function BF_PerpendicularEdge_3F; Support Region[Om]; Entity FacetsOf[All]; }
      EndIf
     }
    Constraint {
      { NameOfCoef un;  EntityType NodesOf ; NameOfConstraint BlochX; }
      If(flag_og==1)
        { NameOfCoef un2e; EntityType EdgesOf ; NameOfConstraint BlochX; }
      EndIf
      If(flag_o2i==1)
        { NameOfCoef un3f; EntityType EdgesOf  ; NameOfConstraint BlochX; }
      EndIf
    }
  }

  { Name Hgrad; Type Form0;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Node_1N; Support Region[Om]; Entity NodesOf[All]; }
      If(flag_og==1) // for curved elements, mid-edge nodes are already included in BF_PerpendicularEdge_1N
        { Name sn2e; NameOfCoef un2e; Function BF_Node_2E; Support Region[Om]; Entity EdgesOf[All]; }
      EndIf
      If(flag_o2i==1)
        { Name sn3e; NameOfCoef un3e; Function BF_Node_3E; Support Region[Om]; Entity EdgesOf[All]; }
        { Name sn3f; NameOfCoef un3f; Function BF_Node_3F; Support Region[Om]; Entity FacetsOf[All]; }
      EndIf
     }
    Constraint {
      { NameOfCoef un;  EntityType NodesOf ; NameOfConstraint BlochX; }
      If(flag_og==1)
        { NameOfCoef un2e; EntityType EdgesOf ; NameOfConstraint BlochX; }
      EndIf
      If(flag_o2i==1)
        { NameOfCoef un3f; EntityType EdgesOf  ; NameOfConstraint BlochX; }
      EndIf
    }
  }

  { Name Hcurl; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Edge   ; Support Region[Om]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[Om]; Entity EdgesOf[All]; }
      If(flag_o2i==1)
        { Name sn3; NameOfCoef un3; Function BF_Edge_3F_a; Support Region[Om]; Entity FacetsOf[All]; }
        { Name sn4; NameOfCoef un4; Function BF_Edge_3F_b; Support Region[Om]; Entity FacetsOf[All]; }
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[Om]; Entity EdgesOf[All]; }
        // BF_Edge_3F_a, BF_Edge_3F_b, BF_Edge_4E
      EndIf
    }
    Constraint {
      { NameOfCoef un;  EntityType EdgesOf ; NameOfConstraint BlochX; }
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint BlochX; }
      { NameOfCoef un;  EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      If(flag_o2i==1)
        { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint BlochX; }
        { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint BlochX; }
        { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint BlochX; }
        { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet; }
        { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint Dirichlet; }
        { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet; }
      EndIf
    }
  }

  { Name Hcurl_aD; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Edge   ; Support Region[Om_1]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[Om_1]; Entity EdgesOf[All]; }
      If(flag_o2i==1)
        { Name sn3; NameOfCoef un3; Function BF_Edge_3F_a; Support Region[Om_1]; Entity FacetsOf[All]; }
        { Name sn4; NameOfCoef un4; Function BF_Edge_3F_b; Support Region[Om_1]; Entity FacetsOf[All]; }
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[Om_1]; Entity EdgesOf[All]; }
        // BF_Edge_3F_a, BF_Edge_3F_b, BF_Edge_4E
      EndIf
    }
  }

  { Name Hcurl_1; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Edge   ; Support Region[{Om_1,Bound}]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[{Om_1,Bound}]; Entity EdgesOf[All]; }
      If(flag_o2i==1)
        { Name sn3; NameOfCoef un3; Function BF_Edge_3F_a; Support Region[{Om_1,Bound}]; Entity FacetsOf[All]; }
        { Name sn4; NameOfCoef un4; Function BF_Edge_3F_b; Support Region[{Om_1,Bound}]; Entity FacetsOf[All]; }
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[{Om_1,Bound}]; Entity EdgesOf[All]; }
      EndIf
    }
  }
  { Name Hcurl_2; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Edge   ; Support Region[{Om_2,Bound}]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[{Om_2,Bound}]; Entity EdgesOf[All]; }
      If(flag_o2i==1)
        { Name sn3; NameOfCoef un3; Function BF_Edge_3F_a; Support Region[{Om_2,Bound}]; Entity FacetsOf[All]; }
        { Name sn4; NameOfCoef un4; Function BF_Edge_3F_b; Support Region[{Om_2,Bound}]; Entity FacetsOf[All]; }
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[{Om_2,Bound}]; Entity EdgesOf[All]; }
      EndIf
    }
    Constraint {
      { NameOfCoef un;  EntityType EdgesOf ; NameOfConstraint BlochX; }
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint BlochX; }
      { NameOfCoef un;  EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      { NameOfCoef un2; EntityType EdgesOf ; NameOfConstraint Dirichlet; }
      If(flag_o2i==1)
        { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint BlochX; }
        { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint BlochX; }
        { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint BlochX; }
        { NameOfCoef un3; EntityType FacetsOf ; NameOfConstraint Dirichlet; }
        { NameOfCoef un4; EntityType FacetsOf ; NameOfConstraint Dirichlet; }
        { NameOfCoef un5; EntityType EdgesOf  ; NameOfConstraint Dirichlet; }
      EndIf
    }
  }
  { Name Hcurl_l; Type Form1;
    BasisFunction {
      { Name sn;  NameOfCoef un;  Function BF_Edge   ; Support Region[Bound]; Entity EdgesOf[All]; }
      { Name sn2; NameOfCoef un2; Function BF_Edge_2E; Support Region[Bound]; Entity EdgesOf[All]; }
      If(flag_o2i==1)
        { Name sn5; NameOfCoef un5; Function BF_Edge_4E  ; Support Region[Bound]; Entity EdgesOf[All]; }
      EndIf
    }
      Constraint {
    }
  }

}

Formulation {
  { Name modal_Aux_E; Type FemEquation;
    Quantity {
      { Name e  ; Type Local; NameOfSpace Hcurl   ;}
      { Name eaD; Type Local; NameOfSpace Hcurl_aD;}
    }
    Equation {
      Galerkin{    [ cel^2/mur[]    * Dof{Curl e}, {Curl e} ] ;           In Om   ; Jacobian JSur; Integration Int_1;}
      Galerkin{ Eig[ epsr_nod[]     * Dof{e}     , {e}      ] ; Order 2 ; In Om   ; Jacobian JSur; Integration Int_1;}
      Galerkin{    [ Sqrt[2]*om_d_1 * Dof{e}     , {eaD}    ] ;           In Om_1 ; Jacobian JSur; Integration Int_1;}
      Galerkin{    [ gam_1          * Dof{eaD}   , {eaD}    ] ;           In Om_1 ; Jacobian JSur; Integration Int_1;}
      Galerkin{ Eig[ Om_D[]         * Dof{eaD }  , {e}      ] ; Order 1 ; In Om_1 ; Jacobian JSur; Integration Int_1;}
      Galerkin{ Eig[                 -Dof{eaD}   , {eaD}    ] ; Order 1 ; In Om_1 ; Jacobian JSur; Integration Int_1;}
    }
  }
  { Name modal_helmholtz_Lag_E; Type FemEquation;
    Quantity {
      { Name u_1   ; Type Local ; NameOfSpace Hcurl_1 ;}
      { Name u_2   ; Type Local ; NameOfSpace Hcurl_2 ;}
      { Name lambda; Type Local ; NameOfSpace Hcurl_l ;}
    }
    Equation {
      Galerkin {    [   1.* term_o3_0rr_1[]/mur[]* Dof{Curl u_1},{Curl u_1}];          In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-I[] * term_o3_1cc_1[]      * Dof{u_1}     ,{u_1}     ]; Order 1; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-I[] * term_o3_1rr_1[]/mur[]* Dof{Curl u_1},{Curl u_1}]; Order 1; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[  -1.* term_o3_2cc_1[]      * Dof{u_1}     ,{u_1}     ]; Order 2; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ I[] * term_o3_3cc_1[]      * Dof{u_1}     ,{u_1}     ]; Order 3; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin {    [  1. * term_o3_0rr_2[]/mur[]* Dof{Curl u_2},{Curl u_2}];          In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-I[] * term_o3_1cc_2[]      * Dof{u_2}     ,{u_2}     ]; Order 1;In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-I[] * term_o3_1rr_2[]/mur[]* Dof{Curl u_2},{Curl u_2}]; Order 1;In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[  -1 * term_o3_2cc_2[]      * Dof{u_2}     ,{u_2}     ]; Order 2;In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ I[] * term_o3_3cc_2[]      * Dof{u_2}     ,{u_2}     ]; Order 3;In Om_2; Jacobian JSur; Integration Int_1;}
      // The actual value of the lagrange multiplier is irrelevant => scaled to balance the eigenvectors
      Galerkin {   [   1. *  term_o3_0rr_1[] * 1e8 * Dof{lambda} , {u_1   } ] ;          In  Bound; Jacobian JLin ; Integration Int_1;}
      Galerkin {   [   1. * -term_o3_0rr_2[] * 1e8 * Dof{lambda} , {u_2   } ] ;          In  Bound; Jacobian JLin ; Integration Int_1;}
      Galerkin {Eig[ -I[] *  term_o3_1rr_1[] * 1e8 * Dof{lambda} , {u_1   } ] ; Order 1; In  Bound; Jacobian JLin ; Integration Int_1;}
      Galerkin {Eig[ -I[] * -term_o3_1rr_2[] * 1e8 * Dof{lambda} , {u_2   } ] ; Order 1; In  Bound; Jacobian JLin ; Integration Int_1;}
      Galerkin {   [         Dof{u_1}        * 1e8               , {lambda} ] ;          In  Bound; Jacobian JLin ; Integration Int_1;}
      Galerkin {   [        -Dof{u_2}        * 1e8               , {lambda} ] ;          In  Bound; Jacobian JLin ; Integration Int_1;}
    }
  }
  { Name modal_helmholtz_PEP_E; Type FemEquation;
    Quantity {{ Name u   ; Type Local; NameOfSpace Hcurl  ;}}
    Equation {
      Galerkin {    [-cel^2/mur[]*gam_1*Dof{Curl u},{Curl u}];          In Om  ; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ cel^2/mur[]      *Dof{Curl u},{Curl u}]; Order 1; In Om  ; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ om_d_1^2         *Dof{u}     ,{u}     ]; Order 1; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-eps_oo_1*gam_1   *Dof{u}     ,{u}     ]; Order 2; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ eps_oo_1         *Dof{u}     ,{u}     ]; Order 3; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-epsr_nod[]*gam_1 *Dof{u}     ,{u}     ]; Order 2; In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ epsr_nod[]       *Dof{u}     ,{u}     ]; Order 3; In Om_2; Jacobian JSur; Integration Int_1;}
    }
  }
  {Name modal_helmholtz_NEP_E; Type FemEquation;
    Quantity {{ Name u   ; Type Local; NameOfSpace Hcurl  ;}}
    Equation {
        Galerkin { Eig[   1/mur[] *    Dof{Curl u}, {Curl u} ]; Rational 1; In Om ; Jacobian JSur; Integration Int_1;}
        Galerkin { Eig[ -epsr_nod[]/cel^2 * Dof{u}, {u}      ]; Rational 2; In Om_1; Jacobian JSur; Integration Int_1;}
        Galerkin { Eig[ -epsr_nod[]/cel^2 * Dof{u}, {u}      ]; Rational 3; In Om_2; Jacobian JSur; Integration Int_1;}
    }
  }
  {Name modal_helmholtz_PEP_h; Type FemEquation;
    // Quantity {{ Name u   ; Type Local; NameOfSpace Hgrad_perp  ;}}
    Quantity {{ Name u   ; Type Local; NameOfSpace Hgrad  ;}}
    Equation {
      Galerkin {    [-1/epsr_nod[]* om_d_1^2     * Dof{d u}, {d u} ];          In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ 1/epsr_nod[]*eps_oo_1*gam_1* Dof{d u}, {d u} ]; Order 1; In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-1/epsr_nod[]*eps_oo_1      * Dof{d u}, {d u} ]; Order 2; In Om_2; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ 1/epsr_nod[]*gam_1         * Dof{d u}, {d u} ]; Order 1; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-1/epsr_nod[]               * Dof{d u}, {d u} ]; Order 2; In Om_1; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-mur[]/cel^2 *om_d_1^2      *  Dof{u},    {u} ]; Order 2; In Om  ; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[ mur[]/cel^2 *eps_oo_1*gam_1*  Dof{u},    {u} ]; Order 3; In Om  ; Jacobian JSur; Integration Int_1;}
      Galerkin { Eig[-mur[]/cel^2 *eps_oo_1      *  Dof{u},    {u} ]; Order 4; In Om  ; Jacobian JSur; Integration Int_1;}
    }
  }
}

Resolution {
  { Name res_PEP_E;
    System{
        { Name M1; NameOfFormulation modal_helmholtz_PEP_E    ; Type ComplexValue; }
    }
    Operation{
      GenerateSeparate[M1];EigenSolve[M1,neig,eig_target_re,eig_target_im];
      // Print[M1];
    }
  }
  { Name res_NEP_E;
    System{
        { Name M1; NameOfFormulation modal_helmholtz_NEP_E ; Type ComplexValue; }
    }
    Operation{
      GenerateSeparate[M1];
      // //  Fan rational (iw)
      // (2*Pi*cel/lambda_vp)
        EigenSolve[M1,neig,eig_target_re,eig_target_im,EigFilter[],
          { {1}, {-eps_oo_1,gam_1*eps_oo_1, -om_d_1^2,0}, {-1,0,0} } ,
          { {1}, {1,-gam_1},                              {1} } ];
    }
  }

  { Name res_Lag_E;
    System{
      { Name M1; NameOfFormulation modal_helmholtz_Lag_E ; Type ComplexValue; }
    }
    Operation{
      GenerateSeparate[M1]; EigenSolve[M1,neig,eig_target_re,eig_target_im]; // Print[M1];
    }
  }
  { Name res_PEP_h;
    System{
      { Name M1; NameOfFormulation modal_helmholtz_PEP_h    ; Type ComplexValue; }
    }
    Operation{
      GenerateSeparate[M1];
      EigenSolve[M1,neig,eig_target_re,eig_target_im];  Print[M1];
    }
  }

  { Name res_Aux_E;
    System{
      { Name M1; NameOfFormulation modal_Aux_E ; Type ComplexValue; }
    }
    Operation{
      GenerateSeparate[M1]; EigenSolve[M1,neig,eig_target_re,eig_target_im,EigFilter[]];
    }
  }
}

PostProcessing {
  { Name postpro_eigenvectors_PEP_E; NameOfFormulation modal_helmholtz_PEP_E;
    Quantity {
      { Name intnormu_pml   ; Value { Integral { [ Norm[{u}] ] ; In Om_pml  ; Jacobian JSur; Integration Int_1 ;} } }
      { Name intnormu_nopml ; Value { Integral { [ Norm[{u}] ] ; In Om_nopml; Jacobian JSur; Integration Int_1 ;} } }
      { Name u   ; Value { Local { [ {u}    ] ; In Om; Jacobian JSur; } } }

      { Name EigenValuesReal;  Value { Local{ [$EigenvalueReal]; In PrintPoint; Jacobian JSur; } } }
      { Name EigenValuesImag;  Value { Local{ [$EigenvalueImag]; In PrintPoint; Jacobian JSur; } } }
    }
  }
  { Name postpro_eigenvectors_NEP_E; NameOfFormulation modal_helmholtz_NEP_E;
    Quantity {
      { Name u   ; Value { Local { [ {u}    ] ; In Om; Jacobian JSur; } } }
      { Name EigenValuesReal;  Value { Local{ [$EigenvalueReal]; In PrintPoint; Jacobian JSur; } } }
      { Name EigenValuesImag;  Value { Local{ [$EigenvalueImag]; In PrintPoint; Jacobian JSur; } } }
    }
  }
  { Name postpro_eigenvectors_Lag_E; NameOfFormulation modal_helmholtz_Lag_E;
    Quantity {
      { Name u_1    ; Value { Local { [ {u_1}    ] ; In Om_1; Jacobian JSur; } } }
      { Name u_2    ; Value { Local { [ {u_2}    ] ; In Om_2; Jacobian JSur; } } }
      { Name lambda ; Value { Local { [ {lambda} ] ; In Bound  ; Jacobian JLin; } } }
      { Name EigenValuesReal;  Value { Local{ [$EigenvalueReal]; In PrintPoint; Jacobian JSur; } } }
      { Name EigenValuesImag;  Value { Local{ [$EigenvalueImag]; In PrintPoint; Jacobian JSur; } } }
    }
  }
  { Name postpro_eigenvectors_PEP_h; NameOfFormulation modal_helmholtz_PEP_h;
    Quantity {
      { Name u   ; Value { Local { [ {u}    ] ; In Om; Jacobian JSur; } } }
      { Name EigenValuesReal;  Value { Local{ [$EigenvalueReal]; In PrintPoint; Jacobian JSur; } } }
      { Name EigenValuesImag;  Value { Local{ [$EigenvalueImag]; In PrintPoint; Jacobian JSur; } } }
    }
  }
  { Name postpro_eigenvectors_Aux_E; NameOfFormulation modal_Aux_E;
    Quantity {
      { Name e    ; Value { Local { [ {e}    ] ; In Om; Jacobian JSur; } } }
      { Name eaD ; Value { Local { [ {eaD} ] ; In Om_1; Jacobian JSur; } } }
      { Name EigenValuesReal;  Value { Local{ [$EigenvalueReal]; In PrintPoint; Jacobian JSur; } } }
      { Name EigenValuesImag;  Value { Local{ [$EigenvalueImag]; In PrintPoint; Jacobian JSur; } } }
    }
  }
}

PostOperation {
  { Name postop_PEP_E; NameOfPostProcessing postpro_eigenvectors_PEP_E ;
    Operation {
      Print [EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesReal.dat"];
      Print [EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesImag.dat"];
      If (flag_outEigvec==1)
        Print [ u   , OnElementsOf Om,File "u_PEP_E.pos", Name "Electric eigen-field (PEP-E)", EigenvalueLegend ];
      EndIf
    }
  }
  { Name postop_NEP_E; NameOfPostProcessing postpro_eigenvectors_NEP_E ;
    Operation {
      Print [EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesReal.dat"];
      Print [EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesImag.dat"];
      If (flag_outEigvec==1)
        Print [ u   , OnElementsOf Om,File "u_NEP_E.pos", Name "Electric eigen-field NEP-E", EigenvalueLegend];
      EndIf
    }
  }
  { Name postop_Lag_E; NameOfPostProcessing postpro_eigenvectors_Lag_E ;
    Operation {
      Print [EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesReal.dat"];
      Print [EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesImag.dat"];
      If (flag_outEigvec==1)
        Print [ lambda , OnElementsOf Bound, File "lam_Lag_E.pos", Name "Lagrange multiplier (Lag-E)"];
        Print [ u_1    , OnElementsOf Om_1 , File "u1_Lag_E.pos", Name "Electric eigen-field 1 (Lag-E)"];
        Print [ u_2    , OnElementsOf Om_2 , File "u2_Lag_E.pos", Name "Electric eigen-field 2 (Lag-E)", EigenvalueLegend];
      EndIf
    }
  }
  { Name postop_PEP_h; NameOfPostProcessing postpro_eigenvectors_PEP_h ;
    Operation {
      Print [EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesReal.dat"];
      Print [EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesImag.dat"];
      If (flag_outEigvec==1)
        Print [ u   , OnElementsOf Om, File "u_PEP_h.pos", Name "Magnetic eigen-field (PEP-h)", EigenvalueLegend];
      EndIf
    }
  }
  { Name postop_Aux_E; NameOfPostProcessing postpro_eigenvectors_Aux_E ;
    Operation {
      Print [EigenValuesReal, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesReal.dat"];
      Print [EigenValuesImag, OnElementsOf PrintPoint, Format TimeTable, File "EigenValuesImag.dat"];
      If (flag_outEigvec==1)
        Print [ eaD , OnElementsOf Om_1, File "u_Aux_EaD.pos", Name "Auxiliary Field Aux-E"];
        Print [ e   , OnElementsOf Om  , File "u_Aux_E.pos", Name "E Aux-E", EigenvalueLegend];
      EndIf
    }
  }
}

eig_tol = 1e-6;
slepc_options_nep_rat = Sprintf(" -nep_view ");
// // These slepc options are now by default (see Kernel/EigenSolve_SLEPC.cpp for default options)
// slepc_options_st  = " -st_type  sinvert -st_ksp_type preonly -st_pc_type lu -st_pc_factor_mat_solver_type mumps ";
// slepc_options_pep = Sprintf(" -pep_max_it 400 -pep_target_magnitude -pep_tol %.10f -pep_view ",eig_tol);
slepc_options_st  = " ";
slepc_options_pep = Sprintf(" -pep_view ");

If (flag_res==0)
  which_res="Aux_E";
  str_slepc_options=StrCat(slepc_options_st,slepc_options_pep);
EndIf

If (flag_res==1)
  which_res="PEP_E";
  str_slepc_options=StrCat(slepc_options_st,slepc_options_pep);
EndIf

If (flag_res==2)
  which_res="NEP_E";
  str_slepc_options=StrCat(slepc_options_nep_rat);
EndIf

If (flag_res==3)
  which_res="Lag_E";
  str_slepc_options=StrCat(slepc_options_st,slepc_options_pep);
EndIf

If (flag_res==4)
  which_res="PEP_h";
  str_slepc_options=StrCat(slepc_options_st,slepc_options_pep);
EndIf

// // redirect converged eigenvalues at runtime to some textfile
// str_slepc_options = StrCat(str_slepc_options," -nep_monitor_all :temp_eigenvalues.txt ");

DefineConstant[
 R_ = {StrCat("res_",which_res), Name "GetDP/1ResolutionChoices", Visible 1},
 C_ = {Sprintf(StrCat("-solve -pos -slepc -petsc_prealloc 200 ",str_slepc_options,slepc_options_rg)), Name "GetDP/9ComputeCommand", Visible 1},
 P_ = {StrCat("postop_",which_res), Name "GetDP/2PostOperationChoices", Visible 1}];
