// Copyright (C) 2018 Guillaume Demésy
//
// This file is part of the model NonLinearEVP.pro.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with This program. If not, see <https://www.gnu.org/licenses/>.

Include "NonLinearEVP_data.geo";
lc_cell      = a_lat/paramaille;
lc_sq        = lc_cell/2;
lc_pml       = lc_cell*1.2;
lc_sqa       = lc_sq;
lc_sq_inside = lc_sq*1.4;
epsc         = lc_sq*0.9;
refinec      = 1;

If (flag_geom==1)
  If (flag_round==1)
      Point(1)   = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
      Point(2)   = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
      Point(3)   = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
      Point(4)   = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
      Point(5)   = {-d_sq/2.+corner_rad ,-d_sq/2.            , 0. , lc_sq/refinec};
      Point(6)   = {-d_sq/2.             ,-d_sq/2.+corner_rad, 0. , lc_sq/refinec};
      Point(8)   = { d_sq/2.-corner_rad ,-d_sq/2.            , 0. , lc_sq/refinec};
      Point(9)   = { d_sq/2.             ,-d_sq/2.+corner_rad, 0. , lc_sq/refinec};
      Point(11)  = { d_sq/2.-corner_rad , d_sq/2.            , 0. , lc_sq/refinec};
      Point(12)  = { d_sq/2.             , d_sq/2.-corner_rad, 0. , lc_sq/refinec};
      Point(14)  = {-d_sq/2.+corner_rad , d_sq/2.            , 0. , lc_sq/refinec};
      Point(15)  = {-d_sq/2.             , d_sq/2.-corner_rad, 0. , lc_sq/refinec};
      // center
      Point(7)   = {-d_sq/2.+corner_rad ,-d_sq/2.+corner_rad, 0. , lc_sq/(refinec)};
      Point(10)  = { d_sq/2.-corner_rad ,-d_sq/2.+corner_rad, 0. , lc_sq/(refinec)};
      Point(13)  = { d_sq/2.-corner_rad , d_sq/2.-corner_rad, 0. , lc_sq/(refinec)};
      Point(16)  = {-d_sq/2.+corner_rad , d_sq/2.-corner_rad, 0. , lc_sq/(refinec)};

      Point(17) = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
      Point(18) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
      Point(19) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
      Point(20) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};

      Point(21)  = {-(d_sq/2.+(Sqrt[2]/2-1)*corner_rad) , -(d_sq/2.+(Sqrt[2]/2-1)*corner_rad), 0. , lc_sq/refinec};
      Point(22)  = { (d_sq/2.+(Sqrt[2]/2-1)*corner_rad) , -(d_sq/2.+(Sqrt[2]/2-1)*corner_rad), 0. , lc_sq/refinec};
      Point(23)  = { (d_sq/2.+(Sqrt[2]/2-1)*corner_rad) , (d_sq/2.+(Sqrt[2]/2-1)*corner_rad), 0.  , lc_sq/refinec};
      Point(24)  = {-(d_sq/2.+(Sqrt[2]/2-1)*corner_rad) , (d_sq/2.+(Sqrt[2]/2-1)*corner_rad), 0.  , lc_sq/refinec};

      Point(25)  = {-(d_sq/2) , 0, 0. , lc_sq};
      Point(26)  = { (d_sq/2) , 0, 0. , lc_sq};
      Point(27)  = {   0  ,  (d_sq/2.), 0.  , lc_sq};
      Point(28)  = {   0  ,  (-d_sq/2.), 0.  , lc_sq};
      Point(29)  = {0,0 , 0.  , lc_sq};

      Circle(15) = {15, 16, 24};
      Circle(16) = {24, 16, 14};
      Circle(17) = {11, 13, 23};
      Circle(18) = {23, 13, 12};
      Circle(19) = {9, 10, 22};
      Circle(20) = {22, 10, 8};
      Circle(21) = {5, 7, 21};
      Circle(22) = {21, 7, 6};

      Line(1) = {20, 19};
      Line(2) = {4, 3};
      Line(3) = {1, 2};
      Line(4) = {17, 18};
      Line(5) = {17, 1};
      Line(6) = {1, 4};
      Line(7) = {4, 20};
      Line(8) = {18, 2};
      Line(9) = {2, 3};
      Line(10) = {3, 19};
      Line(23) = {14, 27};
      Line(24) = {27, 11};
      Line(25) = {12, 26};
      Line(26) = {26, 9};
      Line(27) = {8, 28};
      Line(28) = {28, 5};
      Line(29) = {6, 25};
      Line(30) = {25, 15};
      Line Loop(1) = {7, 1, -10, -2};
      Plane Surface(1) = {1};
      Curve Loop(2) = {30, 15, 16, 23, 24, 17, 18, 25, 26, 19, 20, 27, 28, 21, 22, 29};
      Plane Surface(2) = {2};
      Line Loop(3) = {3, 9, -2, -6};
      Plane Surface(3) = {2, -3};
      Line Loop(4) = {5, 3, -8, -4};
      Plane Surface(4) = {4};
      Point{7,10,13,16,29} In Surface{2};
      
      Periodic Line {8,9,10} = {5,6,7} Translate {a_lat,0,0} ;

      Physical Surface(100) = {2};   // 1 dom in
      Physical Surface(101) = {3};  // 2 dom out
      Physical Surface(102) = {1};  // PML top
      Physical Surface(103) = {4};  // PML bot
      Physical Line(1001)   = {5,6,7}; // bloch x left
      Physical Line(1002)   = {8,9,10}; // bloch x right
      Physical Line(1003)   = {1}; // top bound
      Physical Line(1004)   = {4}; // bot bound
      Physical Line(1005)   = {11,12,13,14,15,16,17,18}; // bound for lag
      Physical Point(10000) = {1};   // Printpoint

  Else
      Point(1)  = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
      Point(2)  = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
      Point(3)  = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
      Point(4)  = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};

      Point(5)  = {-d_sq/2. ,-d_sq/2., 0. , lc_sq};
      Point(6)  = { d_sq/2. ,-d_sq/2., 0. , lc_sq};
      Point(7)  = { d_sq/2. , d_sq/2., 0. , lc_sq};
      Point(8)  = {-d_sq/2. , d_sq/2., 0. , lc_sq};

      Point(9)  = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
      Point(10) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
      Point(11) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
      Point(12) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};

      Line(1) = {1, 2};
      Line(2) = {2, 3};
      Line(3) = {3, 4};
      Line(4) = {4, 1};
      Line(5) = {5, 6};
      Line(6) = {6, 7};
      Line(7) = {7, 8};
      Line(8) = {8, 5};

      Line(9) = {4, 12};
      Line(10) = {12, 11};
      Line(11) = {11, 3};
      Line(12) = {1, 9};
      Line(13) = {9, 10};
      Line(14) = {10, 2};
      Line Loop(1) = {12, 13, 14, -1};
      Plane Surface(1) = {-1};
      Line Loop(2) = {5, 6, 7, 8};
      Plane Surface(2) = {2};
      Line Loop(3) = {4, 1, 2, 3};
      Plane Surface(3) = {2, -3};
      Line Loop(4) = {9, 10, 11, 3};
      Plane Surface(4) = {-4};

      // Periodic Line { 14,2,11} = { 12,4,9} ;
      Periodic Line { 14,2,11 } = {12,4,9 } Translate {a_lat,0,0} ; 

      Physical Surface(100) = {2};       // 1 dom in
      Physical Surface(101) = {3};       // 2 dom out
      Physical Surface(102) = {1};       // PML top
      Physical Surface(103) = {4};       // PML bot
      Physical Line(1001)   = {12,4,9};  // bloch x left
      Physical Line(1002)   = {14,2,11}; // bloch x right
      Physical Line(1003)   = {10};      // top bound
      Physical Line(1004)   = {13};      // bot bound
      Physical Line(1005)   = {5,6,7,8}; // bound for lag
      Physical Point(10000) = {1};       // Printpoint

  EndIf
EndIf

If (flag_geom==2)

  Point(1)  = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(2)  = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(3)  = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(4)  = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};

  Point(5)  = {-a_lat/2. ,-d_sq/2, 0. , lc_sq/refinec};
  Point(6)  = { a_lat/2. ,-d_sq/2, 0. , lc_sq/refinec};
  Point(7)  = { a_lat/2. , d_sq/2, 0. , lc_sq/refinec};
  Point(8)  = {-a_lat/2. , d_sq/2, 0. , lc_sq/refinec};

  Point(9)  = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(10) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(11) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(12) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};

  Point(13)  = {-a_lat/2. ,-d_sq/2-space2pml/3, 0. , lc_cell};
  Point(14)  = { a_lat/2. ,-d_sq/2-space2pml/3, 0. , lc_cell};
  Point(15)  = { a_lat/2. , d_sq/2+space2pml/3, 0. , lc_cell};
  Point(16)  = {-a_lat/2. , d_sq/2+space2pml/3, 0. , lc_cell};
  Point(17)  = {-a_lat/2. ,-d_sq/2+d_sq/7, 0. , lc_sq};
  Point(18)  = { a_lat/2. ,-d_sq/2+d_sq/7, 0. , lc_sq};
  Point(19)  = { a_lat/2. , d_sq/2-d_sq/7, 0. , lc_sq};
  Point(20)  = {-a_lat/2. , d_sq/2-d_sq/7, 0. , lc_sq};
  Point(21)  = {       0. , d_sq/2-d_sq/7, 0. , lc_sq};
  Point(22)  = {       0. ,-d_sq/2+d_sq/7, 0. , lc_sq};

  Line(1) = {9, 1};
  Line(2) = {1, 13};
  Line(3) = {13, 5};
  Line(4) = {5, 17};
  Line(5) = {17, 20};
  Line(6) = {20, 8};
  Line(7) = {8, 16};
  Line(8) = {16, 4};
  Line(9) = {4, 12};
  Line(10) = {10, 2};
  Line(11) = {2, 14};
  Line(12) = {14, 6};
  Line(13) = {6, 18};
  Line(14) = {18, 19};
  Line(15) = {19, 7};
  Line(16) = {7, 15};
  Line(17) = {15, 3};
  Line(18) = {3, 11};
  Line(19) = {9, 10};
  Line(20) = {1, 2};
  Line(21) = {5, 6};
  Line(22) = {8, 7};
  Line(23) = {4, 3};
  Line(24) = {12, 11};
  Curve Loop(1) = {1, 20, -10, -19};
  Plane Surface(1) = {1};
  Curve Loop(2) = {2, 3, 21, -12, -11, -20};
  Plane Surface(2) = {2};
  Curve Loop(3) = {5, 6, 22, -15, -14, -13, -21, 4};
  Plane Surface(3) = {3};
  Curve Loop(4) = {8, 23, -17, -16, -22, 7};
  Plane Surface(4) = {4};
  Curve Loop(5) = {23, 18, -24, -9};
  Plane Surface(5) = {5};

  Periodic Line { 10,11,12,13,14,15,16,17,18 } = { 1,2,3,4,5,6,7,8,9 }  Translate {a_lat,0,0} ; 

  Point{21,22} In Surface{3};
  
  Physical Surface('IN',100) = {3};          // 1 dom in
  Physical Surface('OUT',101) = {2,4};        // 2 dom out
  Physical Surface('PMLTOP',102) = {5};          // PML top
  Physical Surface('PMLBOT',103) = {1};          // PML bot
  Physical Line('BXM',1001)   = {1,2,3,4,5,6,7,8,9};  // bloch x left
  Physical Line('BXP',1002)   = {10,11,12,13,14,15,16,17,18}; // bloch x right
  Physical Line(1003)   = {24};         // top bound
  Physical Line(1004)   = {19};         // bot bound
  Physical Line(1005)   = {21,22};      // bound for lag
  Physical Point(10000) = {1};          // Printpoint

EndIf

If (flag_geom==3)
  Point(1)   = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(2)   = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(3)   = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(4)   = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};

  Point(17) = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(18) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(19) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(20) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};

  Point(25)  = {-(d_sq/2) , 0, 0. , lc_sq/refinec};
  Point(26)  = { (d_sq/2) , 0, 0. , lc_sq/refinec};
  Point(27)  = {   0  ,  (d_sq/2.), 0.  , lc_sq/refinec};
  Point(28)  = {   0  ,  (-d_sq/2.), 0.  , lc_sq/refinec};
  Point(29)  = {0,0 , 0.  , lc_sq};


  Line(1) = {20, 19};
  Line(2) = {4, 3};
  Line(3) = {1, 2};
  Line(4) = {17, 18};
  Line(5) = {17, 1};
  Line(6) = {1, 4};
  Line(7) = {4, 20};
  Line(8) = {18, 2};
  Line(9) = {2, 3};
  Line(10) = {3, 19};
  Circle(11) = {25, 29, 27};
  Circle(12) = {27, 29, 26};
  Circle(13) = {26, 29, 28};
  Circle(14) = {28, 29, 25};

  Line Loop(1) = {7, 1, -10, -2};
  Plane Surface(1) = {1};
  Curve Loop(2) = {11, 12, 13, 14};
  Plane Surface(2) = {2};
  Curve Loop(3) = {6, 2, -9, -3};
  Plane Surface(3) = {2, 3};
  Line Loop(4) = {5, 3, -8, -4};
  Plane Surface(4) = {4};
  
  Periodic Line {8,9,10} = {5,6,7} Translate {a_lat,0,0} ;

  Physical Surface(100) = {2};   // 1 dom in
  Physical Surface(101) = {3};  // 2 dom out
  Physical Surface(102) = {1};  // PML top
  Physical Surface(103) = {4};  // PML bot
  Physical Line(1001)   = {5,6,7}; // bloch x left
  Physical Line(1002)   = {8,9,10}; // bloch x right
  Physical Line(1003)   = {1}; // top bound
  Physical Line(1004)   = {4}; // bot bound
  Physical Line(1005)   = {11,12,13,14}; // bound for lag
  Physical Point(10000) = {1};   // Printpoint
EndIf

If (flag_geom==4)
  Point(1)  = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(2)  = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(3)  = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(4)  = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};

  Point(5)  = {-a_lat/2. ,-d_sq/2, 0. , lc_sq/refinec};
  Point(6)  = { a_lat/2. ,-d_sq/2, 0. , lc_sq/refinec};
  Point(7)  = { a_lat/2. , d_sq/2, 0. , lc_sq/refinec};
  Point(8)  = {-a_lat/2. , d_sq/2, 0. , lc_sq/refinec};

  Point(9)  = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(10) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(11) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(12) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};

  Point(13)  = {-a_lat/2. ,-d_sq/2-1.*lc_sq, 0. , lc_sq};
  Point(14)  = { a_lat/2. ,-d_sq/2-1.*lc_sq, 0. , lc_sq};
  Point(15)  = { a_lat/2. , d_sq/2+1.*lc_sq, 0. , lc_sq};
  Point(16)  = {-a_lat/2. , d_sq/2+1.*lc_sq, 0. , lc_sq};
  Point(17)  = {-a_lat/2. ,-d_sq/2+1.*lc_sq, 0. , lc_sq};
  Point(18)  = { a_lat/2. ,-d_sq/2+1.*lc_sq, 0. , lc_sq};
  Point(19)  = { a_lat/2. , d_sq/2-1.*lc_sq, 0. , lc_sq};
  Point(20)  = {-a_lat/2. , d_sq/2-1.*lc_sq, 0. , lc_sq};
  // Point(21)  = {       0. , d_sq/2-d_sq/7, 0. , lc_sq};
  // Point(22)  = {       0. ,-d_sq/2+d_sq/7, 0. , lc_sq};

  Line(1) = {9, 1};
  Line(2) = {1, 13};
  Line(3) = {13, 5};
  Line(4) = {5, 17};
  Line(5) = {17, 20};
  Line(6) = {20, 8};
  Line(7) = {8, 16};
  Line(8) = {16, 4};
  Line(9) = {4, 12};
  Line(10) = {10, 2};
  Line(11) = {2, 14};
  Line(12) = {14, 6};
  Line(13) = {6, 18};
  Line(14) = {18, 19};
  Line(15) = {19, 7};
  Line(16) = {7, 15};
  Line(17) = {15, 3};
  Line(18) = {3, 11};
  Line(19) = {9, 10};
  Line(20) = {1, 2};
  Line(21) = {5, 6};
  Line(22) = {8, 7};
  Line(23) = {4, 3};
  Line(24) = {12, 11};

  Line(25) = {13, 14};
  Line(26) = {17, 18};
  Line(27) = {20, 19};
  Line(28) = {16, 15};
  Curve Loop(1) = {1, 20, -10, -19};
  Plane Surface(1) = {1};
  Curve Loop(2) = {2, 25, -11, -20};
  Plane Surface(2) = {2};
  Curve Loop(3) = {3, 21, -12, -25};
  Plane Surface(3) = {3};
  Curve Loop(4) = {4, 26, -13, -21};
  Plane Surface(4) = {4};
  Curve Loop(5) = {5, 27, -14, -26};
  Plane Surface(5) = {5};
  Curve Loop(6) = {6, 22, -15, -27};
  Plane Surface(6) = {6};
  Curve Loop(7) = {7, 28, -16, -22};
  Plane Surface(7) = {7};
  Curve Loop(8) = {8, 23, -17, -28};
  Plane Surface(8) = {8};
  Curve Loop(9) = {9, 24, -18, -23};
  Plane Surface(9) = {9};

  Transfinite Surface { 3 } AlternateLeft;
  Transfinite Surface { 4 } AlternateRight;

  Transfinite Surface { 6 } AlternateLeft;
  Transfinite Surface { 7 } AlternateRight;

  Periodic Line { 10,11,12,13,14,15,16,17,18 } = { 1,2,3,4,5,6,7,8,9 }  Translate {a_lat,0,0} ; 
  // Point{21,22} In Surface{3};

  Physical Surface('IN',100) = {4,5,6};          // 1 dom in
  Physical Surface('OUT',101) = {2,3,7,8};        // 2 dom out
  Physical Surface('PMLTOP',102) = {9};          // PML top
  Physical Surface('PMLBOT',103) = {1};          // PML bot
  Physical Line('BXM',1001)   = {1,2,3,4,5,6,7,8,9};  // bloch x left
  Physical Line('BXP',1002)   = {10,11,12,13,14,15,16,17,18}; // bloch x right
  Physical Line('LTOP',1003)   = {24};         // top bound
  Physical Line('LBOT',1004)   = {19};         // bot bound
  Physical Line('LAG',1005)   = {21,22};      // bound for lag
  Physical Point('PRINT',10000) = {1};          // Printpoint
EndIf

If (flag_geom==5)
  epsc      = lc_sq*0.5;
  lc_sqa    = lc_sq;
  Point(1)  = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(2)  = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(3)  = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(4)  = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(5)  = {-d_sq/2. ,-d_sq/2., 0. , lc_sq*15};
  Point(6)  = { d_sq/2. ,-d_sq/2., 0. , lc_sq*15};
  Point(7)  = { d_sq/2. , d_sq/2., 0. , lc_sq*15};
  Point(8)  = {-d_sq/2. , d_sq/2., 0. , lc_sq*15};
  Point(9)  = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(10) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(11) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(12) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(13) = {-d_sq/2.-epsc , d_sq/2.+epsc, 0. , lc_sqa};
  Point(14) = {-d_sq/2.      , d_sq/2.+epsc, 0. , lc_sqa};
  Point(15) = {-d_sq/2.+epsc , d_sq/2.+epsc, 0. , lc_sqa};
  Point(16) = { d_sq/2.-epsc , d_sq/2.+epsc, 0. , lc_sqa};
  Point(17) = { d_sq/2.      , d_sq/2.+epsc, 0. , lc_sqa};
  Point(18) = { d_sq/2.+epsc , d_sq/2.+epsc, 0. , lc_sqa};
  Point(19) = { d_sq/2.+epsc , d_sq/2.     , 0. , lc_sqa};
  Point(20) = { d_sq/2.+epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(21) = { d_sq/2.+epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(22) = { d_sq/2.+epsc ,-d_sq/2.     , 0. , lc_sqa};
  Point(23) = { d_sq/2.+epsc ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(24) = { d_sq/2.      ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(25) = { d_sq/2.-epsc ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(26) = {-d_sq/2.+epsc ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(27) = {-d_sq/2.      ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(28) = {-d_sq/2.-epsc ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(29) = {-d_sq/2.-epsc ,-d_sq/2.     , 0. , lc_sqa};
  Point(30) = {-d_sq/2.-epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(31) = {-d_sq/2.-epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(32) = {-d_sq/2.-epsc , d_sq/2.     , 0. , lc_sqa};
  Point(33)  = {-d_sq/2.+epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(34)  = { d_sq/2.-epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(35)  = { d_sq/2.-epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(36)  = {-d_sq/2.+epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(37)  = {-d_sq/2.+epsc ,-d_sq/2., 0. , lc_sqa};
  Point(38)  = { d_sq/2.-epsc ,-d_sq/2., 0. , lc_sqa};
  Point(39)  = { d_sq/2.-epsc , d_sq/2., 0. , lc_sqa};
  Point(45)  = {-d_sq/2.+epsc , d_sq/2., 0. , lc_sqa};
  Point(46)  = {-d_sq/2. ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(47)  = { d_sq/2. ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(48)  = { d_sq/2. , d_sq/2.-epsc, 0. , lc_sqa};
  Point(49)  = {-d_sq/2. , d_sq/2.-epsc, 0. , lc_sqa};
  Point(54) = {-a_lat/2. , a_lat/2., 0. , lc_sqa};
  Point(55) = {-a_lat/2. ,-a_lat/2., 0. , lc_sqa};
  Point(56) = { a_lat/2. , a_lat/2., 0. , lc_sqa};
  Point(58) = { a_lat/2. ,-a_lat/2., 0. , lc_sqa};
  Line(1) = {1, 2};
  Line(3) = {3, 4};
  Line(9) = {4, 12};
  Line(10) = {12, 11};
  Line(11) = {11, 3};
  Line(12) = {1, 9};
  Line(13) = {9, 10};
  Line(14) = {10, 2};
  Line(35) = {8, 45};
  Line(36) = {45, 45};
  Line(37) = {39, 39};
  Line(38) = {45, 39};
  Line(39) = {39, 7};
  Line(40) = {7, 48};
  Line(41) = {48, 47};
  Line(42) = {47, 6};
  Line(43) = {6, 38};
  Line(44) = {38, 37};
  Line(45) = {37, 5};
  Line(46) = {5, 46};
  Line(47) = {46, 49};
  Line(48) = {49, 8};
  Line(49) = {31, 49};
  Line(50) = {49, 36};
  Line(51) = {36, 45};
  Line(52) = {45, 15};
  Line(53) = {32, 8};
  Line(54) = {8, 14};
  Line(55) = {13, 8};
  Line(56) = {8, 36};
  Line(57) = {31, 8};
  Line(58) = {8, 15};
  Line(59) = {16, 39};
  Line(60) = {39, 35};
  Line(61) = {35, 48};
  Line(62) = {48, 20};
  Line(63) = {19, 7};
  Line(64) = {7, 17};
  Line(65) = {16, 7};
  Line(66) = {7, 20};
  Line(67) = {35, 7};
  Line(68) = {7, 18};
  Line(69) = {34, 47};
  Line(70) = {47, 21};
  Line(71) = {34, 38};
  Line(72) = {38, 25};
  Line(73) = {25, 6};
  Line(74) = {6, 24};
  Line(75) = {6, 22};
  Line(76) = {6, 21};
  Line(77) = {23, 6};
  Line(78) = {6, 34};
  Line(79) = {30, 46};
  Line(80) = {46, 33};
  Line(81) = {33, 37};
  Line(82) = {37, 26};
  Line(83) = {26, 5};
  Line(84) = {5, 27};
  Line(85) = {5, 28};
  Line(86) = {5, 29};
  Line(87) = {5, 30};
  Line(88) = {5, 33};
  Line(89) = {33, 36};
  Line(90) = {36, 35};
  Line(91) = {35, 34};
  Line(92) = {34, 33};
  Line(93) = {13, 14};
  Line(94) = {14, 15};
  Line(95) = {15, 16};
  Line(96) = {16, 17};
  Line(97) = {17, 18};
  Line(98) = {18, 19};
  Line(99) = {19, 20};
  Line(100) = {20, 21};
  Line(101) = {21, 22};
  Line(102) = {22, 23};
  Line(103) = {23, 24};
  Line(104) = {24, 25};
  Line(105) = {25, 26};
  Line(106) = {26, 27};
  Line(107) = {27, 28};
  Line(108) = {28, 29};
  Line(109) = {29, 30};
  Line(110) = {30, 31};
  Line(111) = {31, 32};
  Line(112) = {32, 13};
  Line(113) = {1, 55};
  Line(114) = {55, 54};
  Line(115) = {54, 4};
  Line(116) = {2, 58};
  Line(117) = {58, 56};
  Line(118) = {56, 3};


  Line Loop(1) = {53, -55, -112};
  Plane Surface(1) = {1};
  Line Loop(2) = {55, 54, -93};
  Plane Surface(2) = {2};
  Line Loop(3) = {54, 94, -58};
  Plane Surface(3) = {-3};
  Line Loop(4) = {35, 52, -58};
  Plane Surface(4) = {4};
  Line Loop(5) = {111, 53, -57};
  Plane Surface(5) = {-5};
  Line Loop(6) = {49, 48, -57};
  Plane Surface(6) = {6};
  Line Loop(7) = {48, 56, -50};
  Plane Surface(7) = {-7};
  Line Loop(8) = {56, 51, -35};
  Plane Surface(8) = {8};
  Line Loop(9) = {51, 38, 60, -90};
  Plane Surface(9) = {-9};
  Line Loop(10) = {38, -59, -95, -52};
  Plane Surface(10) = {10};
  Line Loop(11) = {59, 39, -65};
  Plane Surface(11) = {11};
  Line Loop(12) = {65, 64, -96};
  Plane Surface(12) = {12};
  Line Loop(13) = {64, 97, -68};
  Plane Surface(13) = {-13};
  Line Loop(14) = {68, 98, 63};
  Plane Surface(14) = {-14};
  Line Loop(15) = {63, 66, -99};
  Plane Surface(15) = {15};
  Line Loop(16) = {66, -62, -40};
  Plane Surface(16) = {-16};
  Line Loop(17) = {40, -61, 67};
  Plane Surface(17) = {-17};
  Line Loop(18) = {67, -39, 60};
  Plane Surface(18) = {18};
  Line Loop(19) = {91, 69, -41, -61};
  Plane Surface(19) = {19};
  Line Loop(20) = {70, -100, -62, 41};
  Plane Surface(20) = {20};
  Line Loop(21) = {42, 76, -70};
  Plane Surface(21) = {21};
  Line Loop(22) = {76, 101, -75};
  Plane Surface(22) = {-22};
  Line Loop(23) = {42, 78, 69};
  Plane Surface(23) = {-23};
  Line Loop(24) = {71, -43, 78};
  Plane Surface(24) = {24};
  Line Loop(25) = {72, 73, 43};
  Plane Surface(25) = {25};
  Line Loop(26) = {73, 74, 104};
  Plane Surface(26) = {-26};
  Line Loop(27) = {74, -103, 77};
  Plane Surface(27) = {27};
  Line Loop(28) = {77, 75, 102};
  Plane Surface(28) = {-28};
  Line Loop(29) = {92, 81, -44, -71};
  Plane Surface(29) = {29};
  Line Loop(30) = {82, -105, -72, 44};
  Plane Surface(30) = {30};
  Line Loop(31) = {83, -45, 82};
  Plane Surface(31) = {-31};
  Line Loop(32) = {83, 84, -106};
  Plane Surface(32) = {32};
  Line Loop(33) = {84, 107, -85};
  Plane Surface(33) = {-33};
  Line Loop(34) = {85, 108, -86};
  Plane Surface(34) = {-34};
  Line Loop(35) = {86, 109, -87};
  Plane Surface(35) = {-35};
  Line Loop(36) = {87, 79, -46};
  Plane Surface(36) = {-36};
  Line Loop(37) = {46, 80, -88};
  Plane Surface(37) = {-37};
  Line Loop(38) = {88, 81, 45};
  Plane Surface(38) = {-38};
  Line Loop(39) = {110, 49, -47, -79};
  Plane Surface(39) = {-39};
  Line Loop(40) = {50, -89, -80, 47};
  Plane Surface(40) = {-40};
  Line Loop(43) = {3, 9, 10, 11};
  Plane Surface(43) = {-43};
  Line Loop(44) = {1, -14, -13, -12};
  Plane Surface(44) = {-44};
  Line Loop(45) = {113, 114, 115, -3, -118, -117, -116, -1};
  Line Loop(46) = {106, 107, 108, 109, 110, 111, 112, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105};
  Plane Surface(45) = {-45,-46};
  Line Loop(47) = {89, 90, 91, 92};
  Plane Surface(46) = {-47};
  Periodic Line { 14,116,117,118,11 } = {12,113, 114, 115,9 } Translate {a_lat,0,0} ;
  Periodic Line {47}  = {110} Translate {epsc,0,0} ;
  Periodic Line {89}  = {47}  Translate {epsc,0,0} ;
  Periodic Line {41}  = {91}  Translate {epsc,0,0} ;
  Periodic Line {100} = {41}  Translate {epsc,0,0} ;
  Periodic Line {44} = {105} Translate {0,epsc,0} ;
  Periodic Line {92} = {44}  Translate {0,epsc,0} ;
  Periodic Line {38} = {90}  Translate {0,epsc,0} ;
  Periodic Line {95} = {38}  Translate {0,epsc,0} ;
  Transfinite Surface { 9} Alternate;
  Transfinite Surface {10} AlternateLeft;
  Transfinite Surface {19} Alternate;
  Transfinite Surface {20} AlternateLeft;
  Transfinite Surface {29} Alternate;
  Transfinite Surface {30} AlternateLeft;
  Transfinite Surface {39} Alternate;
  Transfinite Surface {40} AlternateLeft;

  Physical Surface('IN',100) = {7,8,9,17,18,19,23,24,29,37,38,40,46};   // 1 dom in
  Physical Surface('OUT',101) = {45, 1, 2, 3, 4, 10, 5, 6, 39, 36, 35, 34, 33, 32, 31, 30, 25, 26, 27, 28, 22, 21, 20, 16, 15, 14, 13, 12, 11}; // out
  Physical Surface('PMLTOP',102) = {43};  // PML top
  Physical Surface('PMLBOT',103) = {44};  // PML bot
  Physical Line('BXM',1001)   = {12,113,114,115,9}; // bloch x left
  Physical Line('BXP',1002)   = {14,116,117,118,11}; // bloch x right
  Physical Line('TOP',1003)   = {10}; // top bound
  Physical Line('BOT',1004)   = {13}; // bot bound
  Physical Line('LAG',1005)   = {38,39,40,41,42,43,44,45,46,47,48,35}; // bound for lag
  Physical Point('PRINT',10000) = {1};   // Printpoint
EndIf

If (flag_geom==6)
  epsc      = lc_sq/2;
  lc_sqa    = lc_sq;
  s2 = 1-Sqrt(2)/2;
  c8 = epsc*Cos(Pi/8);
  s8 = epsc*Sin(Pi/8);
  Point(1)  = {-a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(2)  = { a_lat/2. ,-d_sq/2.-space2pml, 0. , lc_cell};
  Point(3)  = { a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(4)  = {-a_lat/2. , d_sq/2.+space2pml, 0. , lc_cell};
  Point(5)  = {-d_sq/2.+s2*epsc,-d_sq/2.+s2*epsc, 0. , lc_sq*15};
  Point(6)  = { d_sq/2.-s2*epsc,-d_sq/2.+s2*epsc, 0. , lc_sq*15};
  Point(7)  = { d_sq/2.-s2*epsc, d_sq/2.-s2*epsc, 0. , lc_sq*15};
  Point(8)  = {-d_sq/2.+s2*epsc, d_sq/2.-s2*epsc, 0. , lc_sq*15};
  Point(9)  = {-a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(10) = { a_lat/2. ,-d_sq/2.-space2pml-pmlsize, 0. , lc_pml};
  Point(11) = { a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(12) = {-a_lat/2. , d_sq/2.+space2pml+pmlsize, 0. , lc_pml};
  Point(32) = {-d_sq/2.+epsc-c8*2 , d_sq/2.-epsc+s8*2, 0. , lc_sqa};
  Point(14) = {-d_sq/2.+epsc-s8*2 , d_sq/2.-epsc+c8*2, 0. , lc_sqa};
  Point(15) = {-d_sq/2.+epsc , d_sq/2.+epsc, 0. , lc_sqa};
  Point(16) = { d_sq/2.-epsc , d_sq/2.+epsc, 0. , lc_sqa};
  Point(17) = { d_sq/2.-epsc+s8*2 , d_sq/2.-epsc+c8*2, 0. , lc_sqa};
  Point(19) = { d_sq/2.-epsc+c8*2 , d_sq/2.-epsc+s8*2, 0. , lc_sqa};
  Point(20) = { d_sq/2.+epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(21) = { d_sq/2.+epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(13) = {-d_sq/2.-epsc+2*s2*epsc , d_sq/2.+epsc-2*s2*epsc, 0. , lc_sqa};
  Point(23) = { d_sq/2.+epsc-2*s2*epsc ,-d_sq/2.-epsc+2*s2*epsc, 0. , lc_sqa};
  Point(28) = {-d_sq/2.-epsc+2*s2*epsc ,-d_sq/2.-epsc+2*s2*epsc, 0. , lc_sqa};
  Point(18) = { d_sq/2.+epsc-2*s2*epsc , d_sq/2.+epsc-2*s2*epsc, 0. , lc_sqa};
  Point(24) = { d_sq/2.-epsc+s8*2 ,-d_sq/2.+epsc-c8*2, 0. , lc_sqa};
  Point(22) = { d_sq/2.-epsc+c8*2 ,-d_sq/2.+epsc-s8*2, 0. , lc_sqa};
  Point(25) = { d_sq/2.-epsc ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(26) = {-d_sq/2.+epsc ,-d_sq/2.-epsc, 0. , lc_sqa};
  Point(27) = {-d_sq/2.+epsc-s8*2 ,-d_sq/2.+epsc-c8*2, 0. , lc_sqa};
  Point(29) = {-d_sq/2.+epsc-c8*2 ,-d_sq/2.+epsc-s8*2     , 0. , lc_sqa};


  Point(30) = {-d_sq/2.-epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(31) = {-d_sq/2.-epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(33)  = {-d_sq/2.+epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(34)  = { d_sq/2.-epsc ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(35)  = { d_sq/2.-epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(36)  = {-d_sq/2.+epsc , d_sq/2.-epsc, 0. , lc_sqa};
  Point(37)  = {-d_sq/2.+epsc ,-d_sq/2., 0. , lc_sqa};
  Point(38)  = { d_sq/2.-epsc ,-d_sq/2., 0. , lc_sqa};
  Point(39)  = { d_sq/2.-epsc , d_sq/2., 0. , lc_sqa};
  Point(45)  = {-d_sq/2.+epsc , d_sq/2., 0. , lc_sqa};
  Point(46)  = {-d_sq/2. ,-d_sq/2.+epsc, 0. , lc_sqa};

  Point(47)  = { d_sq/2. ,-d_sq/2.+epsc, 0. , lc_sqa};
  Point(48)  = { d_sq/2. , d_sq/2.-epsc, 0. , lc_sqa};
  Point(49)  = {-d_sq/2. , d_sq/2.-epsc, 0. , lc_sqa};
  Point(54) = {-a_lat/2. , a_lat/2., 0. , lc_sqa};
  Point(55) = {-a_lat/2. ,-a_lat/2., 0. , lc_sqa};
  Point(56) = { a_lat/2. , a_lat/2., 0. , lc_sqa};
  Point(58) = { a_lat/2. ,-a_lat/2., 0. , lc_sqa};
  Line(1) = {1, 2};
  Line(3) = {3, 4};
  Line(9) = {4, 12};
  Line(10) = {12, 11};
  Line(11) = {11, 3};
  Line(12) = {1, 9};
  Line(13) = {9, 10};
  Line(14) = {10, 2};
  // Line(35) = {8, 45};
  Circle(35) = {8, 36,45};

  Line(38) = {45, 39};
  Circle(39) = {39, 35, 7};
  Circle(40) = {7, 35, 48};

  Line(41) = {48, 47};
  Circle(42) = {47, 34, 6};
  Circle(43) = {6, 34, 38};

  Line(44) = {38, 37};
  Circle(46) = {5, 33, 46};
  Circle(45) = {37, 33, 5};

  Line(47) = {46, 49};
  Circle(48) = {49, 36, 8};

  Line(49) = {31, 49};
  Line(50) = {49, 36};
  Line(51) = {36, 45};
  Line(52) = {45, 15};
  Line(59) = {16, 39};
  Line(60) = {39, 35};
  Line(61) = {35, 48};
  Line(62) = {48, 20};
  Line(69) = {34, 47};
  Line(70) = {47, 21};
  Line(71) = {34, 38};
  Line(72) = {38, 25};
  Line(79) = {30, 46};
  Line(80) = {46, 33};
  Line(81) = {33, 37};
  Line(82) = {37, 26};
  Line(89) = {33, 36};
  Line(90) = {36, 35};
  Line(91) = {35, 34};
  Line(92) = {34, 33};
  Line(95) = {15, 16};
  Line(100) = {20, 21};
  Line(105) = {25, 26};
  Line(110) = {30, 31};
  Line(113) = {1, 55};
  Line(114) = {55, 54};
  Line(115) = {54, 4};
  Line(116) = {2, 58};
  Line(117) = {58, 56};
  Line(118) = {56, 3};

  Line Loop(9) = {51, 38, 60, -90};
  Plane Surface(9) = {-9};
  Line Loop(10) = {38, -59, -95, -52};
  Plane Surface(10) = {10};
  Line Loop(19) = {91, 69, -41, -61};
  Plane Surface(19) = {19};
  Line Loop(20) = {70, -100, -62, 41};
  Plane Surface(20) = {20};
  Line Loop(29) = {92, 81, -44, -71};
  Plane Surface(29) = {29};
  Line Loop(30) = {82, -105, -72, 44};
  Plane Surface(30) = {30};
  Line Loop(39) = {110, 49, -47, -79};
  Plane Surface(39) = {-39};
  Line Loop(40) = {50, -89, -80, 47};
  Plane Surface(40) = {-40};
  Line Loop(43) = {3, 9, 10, 11};
  Plane Surface(43) = {-43};
  Line Loop(44) = {1, -14, -13, -12};
  Plane Surface(44) = {-44};
  Line Loop(45) = {113, 114, 115, -3, -118, -117, -116, -1};
  Curve Loop(48) = {48, 35, -51, -50};
  Plane Surface(47) = {48};
  Curve Loop(49) = {60, 61, -40, -39};
  Plane Surface(48) = {49};
  Curve Loop(50) = {71, -43, -42, -69};
  Plane Surface(49) = {50};
  Curve Loop(51) = {80, 81, 45, 46};
  Plane Surface(50) = {51};

  Curve Loop(52) = {95, 59, 39, 40, 62, 100, -70, 42, 43, 72, 105, -82, 45, 46, -79, 110, 49, 48, 35, 52};
  Plane Surface(51) = {45, 52};
  Line Loop(47) = {89, 90, 91, 92};
  Plane Surface(46) = {-47};
  Periodic Line { 14,116,117,118,11 } = {12,113, 114, 115,9 } Translate {a_lat,0,0} ;
  Periodic Line {47}  = {110} Translate {epsc,0,0} ;
  Periodic Line {89}  = {47}  Translate {epsc,0,0} ;
  Periodic Line {41}  = {91}  Translate {epsc,0,0} ;
  Periodic Line {100} = {41}  Translate {epsc,0,0} ;
  Periodic Line {44}  = {105} Translate {0,epsc,0} ;
  Periodic Line {92}  = {44}  Translate {0,epsc,0} ;
  Periodic Line {38}  = {90}  Translate {0,epsc,0} ;
  Periodic Line {95}  = {38}  Translate {0,epsc,0} ;
  Transfinite Surface { 9} AlternateLeft;
  Transfinite Surface {10} Alternate;
  Transfinite Surface {19} Alternate;
  Transfinite Surface {20} Alternate;
  Transfinite Surface {29} Alternate;
  Transfinite Surface {30} Alternate;
  Transfinite Surface {39} Alternate;
  Transfinite Surface {40} AlternateLeft;

  Point {13,14,32 , 17,18,19 , 22,23,24, 27,28,29 } In Surface{51};

  Physical Surface('IN',100) = {7,8,9,17,18,19,23,24,29,37,38,40,46,47,48,49,50};   // 1 dom in
  Physical Surface('OUT',101) = {51,45, 1, 2, 3, 4, 10, 5, 6, 39, 36, 35, 34, 33, 32, 31, 30, 25, 26, 27, 28, 22, 21, 20, 16, 15, 14, 13, 12, 11}; // out
  Physical Surface('PMLTOP',102) = {43};  // PML top
  Physical Surface('PMLBOT',103) = {44};  // PML bot
  Physical Line('BXM',1001)   = {12,113,114,115,9}; // bloch x left
  Physical Line('BXP',1002)   = {14,116,117,118,11}; // bloch x right
  Physical Line('TOP',1003)   = {10}; // top bound
  Physical Line('BOT',1004)   = {13}; // bot bound
  Physical Line('LAG',1005)   = {38,39,40,41,42,43,44,45,46,47,48,35}; // bound for lag
  Physical Point('PRINT',10000) = {1};   // Printpoint
EndIf

Mesh.ElementOrder = flag_og;

